package us.coletnelson.colang.builders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import us.coletnelson.colang.builders.syn.Appendable;
import us.coletnelson.colang.builders.syn.ColangAppendableReplaceable;
import us.coletnelson.colang.builders.syn.ColangAppendableString;
import us.coletnelson.colang.builders.syn.dom.ColangSynModel;
import us.coletnelson.colang.builders.syn.dom.ColangSynPair;
import us.coletnelson.colang.builders.syn.dom.ColangSynPosition;
import us.coletnelson.colang.builders.syn.dom.ColangSynReplacement;

public class ColangSynBuilder {
	private List<Appendable> appendables;
	private List<ColangAppendableReplaceable> replaceables;

	private String currentData;

	public ColangSynBuilder() {
		this.appendables = new ArrayList<>();
		this.replaceables = new ArrayList<>();
		this.resetData();
	}

	public ColangSynBuilder(ColangSynBuilder builder) {
		this.currentData = builder.currentData;
	}

	public void append(String data) {
		this.currentData += data;
	}

	public void appendln(String data) {
		this.currentData += data + "\n";
	}

	public void appendId(String id, int pos, ColangSynPosition synPos) {
		this.appendables.add(new ColangAppendableString(this.currentData));
		this.resetData();
		ColangAppendableReplaceable replaceable = new ColangAppendableReplaceable(
				id, new ArrayList<>(Arrays.asList(id, id + " + 1", id + " - 1")), pos, synPos);
		this.replaceables.add(replaceable);
		this.appendables.add(replaceable);
	}

	public void finalize() {
		this.appendables.add(new ColangAppendableString(this.currentData));
		this.resetData();
	}

	public boolean hasReplaceables() {
		return !this.replaceables.isEmpty();
	}

	public List<ColangSynModel> getReplacementsWithModels() {
		List<ColangSynModel> models = new ArrayList<>();
		if(this.replaceables.size() == 1) {
			ColangAppendableReplaceable replaceable = this.replaceables.get(0);
			for (int i = 0; i < replaceable.getNumberOfForms(); i++) {
				String data = "";
				for (Appendable appendable : this.appendables) {
					data += appendable.getData();
				}
				models.add(new ColangSynModel(data,
						new ColangSynPair(
								new ColangSynReplacement(replaceable.getFoundForm(),
										replaceable.getData(),
										replaceable.getPosition(),
										replaceable.getSynPosition()),
								null), true));
				replaceable.toggleForm();
			}
		} else {
			for (int i = 0; i < this.replaceables.size() - 1; i++) {
				for (int j = i + 1; j < this.replaceables.size(); j++) {
					for (int k = 0; k < this.replaceables.get(i).getNumberOfForms(); k++) {
						for (int h = 0; h < this.replaceables.get(j).getNumberOfForms(); h++) {
							String data = "";
							for (Appendable appendable : this.appendables) {
								data += appendable.getData();
							}
							models.add(new ColangSynModel(data,
									new ColangSynPair(
											new ColangSynReplacement(this.replaceables.get(i).getFoundForm(),
													this.replaceables.get(i).getData(),
													this.replaceables.get(i).getPosition(),
													this.replaceables.get(i).getSynPosition()),
											new ColangSynReplacement(this.replaceables.get(j).getFoundForm(),
													this.replaceables.get(j).getData(),
													this.replaceables.get(j).getPosition(),
													this.replaceables.get(j).getSynPosition()))));
							this.replaceables.get(j).toggleForm();
						}
						this.replaceables.get(i).toggleForm();
					}
				}
			}
		}
		return models;
	}

	public List<String> getNonSynthesizableRepresentation() {
		List<String> strs = new ArrayList<>();
		String data = "";
		for (Appendable appendable : this.appendables) {
			data += appendable.getData();
		}
		strs.add(data);
		return strs;
	}

	public void resetData() {
		this.currentData = "";
	}
}
