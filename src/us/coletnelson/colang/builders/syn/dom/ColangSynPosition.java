package us.coletnelson.colang.builders.syn.dom;

public class ColangSynPosition {
	private int lineNumber;
	private int columnNumber;
	
	public ColangSynPosition(int lineNumber, int columnNumber) {
		this.lineNumber = lineNumber;
		this.columnNumber = columnNumber;
	}

	public int getLineNumber() {
		return lineNumber;
	}
	
	public int getColumnNumber() {
		return columnNumber;
	}
}
