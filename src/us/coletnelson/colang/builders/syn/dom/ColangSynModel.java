package us.coletnelson.colang.builders.syn.dom;

public class ColangSynModel {
	private String data;
	private ColangSynPair pair;
	private boolean isSingle;
	
	public ColangSynModel(String data, ColangSynPair pair) {
		this(data, pair, false);
	}
	
	public ColangSynModel(String data, ColangSynPair pair, boolean isSingle) {
		this.data = data;
		this.pair = pair;
		this.isSingle = isSingle;
	}

	public String getData() {
		return data;
	}
	
	public ColangSynPair getPair() {
		return pair;
	}
	
	public boolean isSingle() {
		return this.isSingle;
	}
}
