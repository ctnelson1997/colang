package us.coletnelson.colang.builders.syn.dom;

public class ColangSynReplacement {
	private int position;
	private String originalData;
	private String newData;
	private ColangSynPosition synPosition;
	
	public ColangSynReplacement(String originalData, String newData, int position, ColangSynPosition synPosition) {
		this.originalData = originalData;
		this.newData = newData;
		this.position = position;
		this.synPosition = synPosition;
	}
	
	public String getOriginalData() {
		return originalData;
	}
	
	public String getNewData() {
		return newData;
	}

	public int getPosition() {
		return position;
	}	
	
	public ColangSynPosition getSynPosition() {
		return this.synPosition;
	}
}
