package us.coletnelson.colang.builders.syn.dom;

public class ColangSynPair {
	private ColangSynReplacement replacement1;
	private ColangSynReplacement replacement2;
	
	public ColangSynPair(ColangSynReplacement replacement1, ColangSynReplacement replacement2) {
		this.replacement1 = replacement1;
		this.replacement2 = replacement2;
	}

	public ColangSynReplacement getReplacement1() {
		return replacement1;
	}

	public ColangSynReplacement getReplacement2() {
		return replacement2;
	}
}
