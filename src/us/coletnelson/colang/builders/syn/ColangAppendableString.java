package us.coletnelson.colang.builders.syn;

public class ColangAppendableString implements Appendable {
	
	private String data;
	
	public ColangAppendableString(String data) {
		this.data = data;
	}

	@Override
	public String getData() {
		return this.data;
	}
}
