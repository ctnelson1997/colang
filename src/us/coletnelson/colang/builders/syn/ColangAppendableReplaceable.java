package us.coletnelson.colang.builders.syn;

import java.util.List;

import us.coletnelson.colang.builders.syn.dom.ColangSynPosition;

public class ColangAppendableReplaceable implements Appendable {

	private int formNumber;
	private String foundForm;
	private List<String> forms;
	private int position;
	private ColangSynPosition synPosition;
	
	public ColangAppendableReplaceable(String foundForm, List<String> forms, int position, ColangSynPosition synPosition) {
		this.foundForm = foundForm;
		this.forms = forms;
		this.formNumber = 0;
		this.position = position;
		this.synPosition = synPosition;
	}
	
	public String getFoundForm() {
		return this.foundForm;
	}
	
	public int getNumberOfForms() {
		return forms.size();
	}
	
	public void toggleForm() {
		this.formNumber = (this.formNumber + 1) % this.forms.size();
	}
	
	public void resetToggle() {
		this.formNumber = 0;
	}
	
	public int getPosition() {
		return this.position;
	}
	
	public ColangSynPosition getSynPosition() {
		return this.synPosition;
	}
	
	@Override
	public String getData() {
		return forms.get(formNumber);
	}
	
}
