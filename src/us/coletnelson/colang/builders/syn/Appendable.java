package us.coletnelson.colang.builders.syn;

public interface Appendable {
	String getData();
}
