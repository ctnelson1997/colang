package us.coletnelson.colang.builders;

public class ColangBuilder {
	
	private String data;
	
	public ColangBuilder() {
		this.reset();
	}
	
	public ColangBuilder(ColangBuilder builder) {
		this.data = builder.data;
	}
	
	public void append(String data) {
		this.data += data;
	}
	
	public void appendln(String data) {
		this.data += data + "\n";
	}
	
	public String get() {
		return this.data;
	}
	
	public String getPretty() {
		String ret = "";
		String[] lines = this.data.split("\n");
		int tabs = 0;
		for(String line : lines) {
			if(line.contains("}")) {
				tabs--;
			}
			for(int i = 0; i < tabs; i++) {
				ret += "\t";
			}
			if(line.contains("{")) {
				tabs++;
			}
			ret += line + "\n";
		}
		return ret;
	}
	
	public void reset() {
		this.data = "";
	}
}
