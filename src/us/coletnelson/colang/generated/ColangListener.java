
package us.coletnelson.colang.generated;

// Generated from Colang.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ColangParser}.
 */
public interface ColangListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ColangParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(ColangParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(ColangParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#stmt}.
	 * @param ctx the parse tree
	 */
	void enterStmt(ColangParser.StmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#stmt}.
	 * @param ctx the parse tree
	 */
	void exitStmt(ColangParser.StmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#cond}.
	 * @param ctx the parse tree
	 */
	void enterCond(ColangParser.CondContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#cond}.
	 * @param ctx the parse tree
	 */
	void exitCond(ColangParser.CondContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#booleanExpr}.
	 * @param ctx the parse tree
	 */
	void enterBooleanExpr(ColangParser.BooleanExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#booleanExpr}.
	 * @param ctx the parse tree
	 */
	void exitBooleanExpr(ColangParser.BooleanExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterExpr(ColangParser.ExprContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitExpr(ColangParser.ExprContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#initStmt}.
	 * @param ctx the parse tree
	 */
	void enterInitStmt(ColangParser.InitStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#initStmt}.
	 * @param ctx the parse tree
	 */
	void exitInitStmt(ColangParser.InitStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#assignStmt}.
	 * @param ctx the parse tree
	 */
	void enterAssignStmt(ColangParser.AssignStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#assignStmt}.
	 * @param ctx the parse tree
	 */
	void exitAssignStmt(ColangParser.AssignStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#inputStmt}.
	 * @param ctx the parse tree
	 */
	void enterInputStmt(ColangParser.InputStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#inputStmt}.
	 * @param ctx the parse tree
	 */
	void exitInputStmt(ColangParser.InputStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#outputStmt}.
	 * @param ctx the parse tree
	 */
	void enterOutputStmt(ColangParser.OutputStmtContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#outputStmt}.
	 * @param ctx the parse tree
	 */
	void exitOutputStmt(ColangParser.OutputStmtContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#whileBlock}.
	 * @param ctx the parse tree
	 */
	void enterWhileBlock(ColangParser.WhileBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#whileBlock}.
	 * @param ctx the parse tree
	 */
	void exitWhileBlock(ColangParser.WhileBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void enterIfBlock(ColangParser.IfBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#ifBlock}.
	 * @param ctx the parse tree
	 */
	void exitIfBlock(ColangParser.IfBlockContext ctx);
	/**
	 * Enter a parse tree produced by {@link ColangParser#elseBlock}.
	 * @param ctx the parse tree
	 */
	void enterElseBlock(ColangParser.ElseBlockContext ctx);
	/**
	 * Exit a parse tree produced by {@link ColangParser#elseBlock}.
	 * @param ctx the parse tree
	 */
	void exitElseBlock(ColangParser.ElseBlockContext ctx);
}