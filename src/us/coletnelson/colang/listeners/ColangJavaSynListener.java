package us.coletnelson.colang.listeners;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import us.coletnelson.colang.builders.ColangSynBuilder;
import us.coletnelson.colang.builders.syn.dom.ColangSynPosition;
import us.coletnelson.colang.generated.ColangBaseListener;
import us.coletnelson.colang.generated.ColangParser;

public class ColangJavaSynListener extends ColangBaseListener {
	
	private ColangSynBuilder builder;
	private int maxItrTimeout;
	private boolean hasErrors;
	
	private Map<String, Integer> idMapping = new HashMap<>();
	
	public ColangJavaSynListener(int maxItrTimeout) {
		this.builder = new ColangSynBuilder();
		this.maxItrTimeout = maxItrTimeout;
		this.hasErrors = false;
	}
	
	public boolean hasErrors() {
		return this.hasErrors;
	}
	
	public ColangSynBuilder getBuilder() {
		return builder;
	}
	
	public void seeId(String id) {
		if(!this.idMapping.containsKey(id)) {
			this.idMapping.put(id, 0);
		}
		this.idMapping.put(id, this.idMapping.get(id) + 1);
	}
	
	public int countId(String id) {
		if(this.idMapping.containsKey(id)) {
			return this.idMapping.get(id);
		} else {
			return 0;
		}
	}
	
	@Override public void enterProg(ColangParser.ProgContext ctx) {
		builder.appendln(String.format("/** Transpiled Colang Program %s **/", new Date().toString()));
		builder.appendln("import java.util.Scanner;");
		builder.appendln("public class ColangProgram {");
		builder.appendln("public static void main(String[] args) {");
		builder.appendln("Scanner sysin = new Scanner(System.in);");
		builder.appendln("int infLoopStopper = 0;");
	}

	@Override public void exitProg(ColangParser.ProgContext ctx) {
		builder.appendln("sysin.close();");
		builder.appendln("}");
		builder.appendln("}");
		builder.finalize();
	}

	@Override public void enterStmt(ColangParser.StmtContext ctx) { }

	@Override public void exitStmt(ColangParser.StmtContext ctx) { }

	@Override public void enterCond(ColangParser.CondContext ctx) {	}

	@Override public void exitCond(ColangParser.CondContext ctx) { }

	@Override public void enterBooleanExpr(ColangParser.BooleanExprContext ctx) {
		if(ctx.TRUE() != null) {
			builder.append("true");
		} else if(ctx.FALSE() != null) {
			builder.append("false");
		}
	}

	@Override public void exitBooleanExpr(ColangParser.BooleanExprContext ctx) { }

	@Override public void enterExpr(ColangParser.ExprContext ctx) {
		if(ctx.ID() != null) {
			this.seeId(ctx.ID().toString());
			builder.appendId(ctx.ID().toString(),
					this.countId(ctx.ID().toString()),
					new ColangSynPosition(ctx.getStart().getLine(), ctx.getStart().getCharPositionInLine()));
		} else if(ctx.NUMBER() != null) {
			builder.append(ctx.NUMBER().toString());
		}
	}

	@Override public void exitExpr(ColangParser.ExprContext ctx) { }

	@Override public void enterInitStmt(ColangParser.InitStmtContext ctx) {
		this.seeId(ctx.ID().toString());
		builder.append("long " + ctx.ID() + " = ");
	}

	@Override public void exitInitStmt(ColangParser.InitStmtContext ctx) {
		builder.appendln(";");
	}

	@Override public void enterAssignStmt(ColangParser.AssignStmtContext ctx) {
		this.seeId(ctx.ID().toString());
		builder.append(ctx.ID() + " = ");
	}

	@Override public void exitAssignStmt(ColangParser.AssignStmtContext ctx) {
		builder.appendln(";");
	}

	@Override public void enterInputStmt(ColangParser.InputStmtContext ctx) {
		this.seeId(ctx.ID().toString());
		builder.appendln(ctx.ID() + " = sysin.nextInt();");
	}

	@Override public void exitInputStmt(ColangParser.InputStmtContext ctx) { }

	@Override public void enterOutputStmt(ColangParser.OutputStmtContext ctx) {
		if(ctx.OUTPUTLN() != null) {
			if(ctx.ID() != null) {
				this.seeId(ctx.ID().toString());
				builder.appendln("System.out.println(" + ctx.ID() + ");" );
			} else if(ctx.NUMBER() != null) {
				builder.appendln("System.out.println(" + ctx.NUMBER() + ");" );
			} else if(ctx.STRING() != null) {
				builder.appendln("System.out.println(" + ctx.STRING().toString().replaceAll("'", "\"") + ");" );
			} else {
				builder.appendln("System.out.println();");
			}
		} else {
			if(ctx.ID() != null) {
				this.seeId(ctx.ID().toString());
				builder.appendln("System.out.print(" + ctx.ID() + ");" );
			} else if(ctx.NUMBER() != null) {
				builder.appendln("System.out.print(" + ctx.NUMBER() + ");" );
			} else if(ctx.STRING() != null) {
				builder.appendln("System.out.print(" + ctx.STRING().toString().replaceAll("'", "\"") + ");" );
			}
		}
		
	}

	@Override public void exitOutputStmt(ColangParser.OutputStmtContext ctx) { }

	@Override public void enterWhileBlock(ColangParser.WhileBlockContext ctx) { 
		builder.append("infLoopStopper = 0;");
		builder.append("while (");
	}

	@Override public void exitWhileBlock(ColangParser.WhileBlockContext ctx) {
		builder.appendln("}");
	}

	@Override public void enterIfBlock(ColangParser.IfBlockContext ctx) {
		builder.append("if(");
	}

	@Override public void exitIfBlock(ColangParser.IfBlockContext ctx) {
		builder.appendln("}");
	}

	@Override public void enterElseBlock(ColangParser.ElseBlockContext ctx) { }

	@Override public void exitElseBlock(ColangParser.ElseBlockContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }

	@Override public void exitEveryRule(ParserRuleContext ctx) { }

	@Override public void visitTerminal(TerminalNode node) {
		if(node.getText().equals("then")) {
			builder.appendln(") { if (infLoopStopper++ > " + this.maxItrTimeout + ") { return; }");
		} else if(node.getText().equals("else")) {
			builder.appendln("} else {");
		} else if(node.getText().equals("<")) {
			builder.append("<");
		} else if(node.getText().equals("<=")) {
			builder.append("<=");
		} else if(node.getText().equals(">")) {
			builder.append(">");
		} else if(node.getText().equals(">=")) {
			builder.append(">=");
		} else if(node.getText().equals("=")) {
			builder.append("==");
		} else if(node.getText().equals("+")) {
			builder.append("+");
		} else if(node.getText().equals("-")) {
			builder.append("-");
		} else if(node.getText().equals("*")) {
			builder.append("*");
		} else if(node.getText().equals("/")) {
			builder.append("/");
		} else if(node.getText().equals("(")) {
			builder.append("(");
		} else if(node.getText().equals(")")) {
			builder.append(")");
		}
	}

	@Override public void visitErrorNode(ErrorNode node) {
		this.hasErrors = true;
	}
}
