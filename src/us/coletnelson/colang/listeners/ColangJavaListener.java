package us.coletnelson.colang.listeners;

import java.util.Date;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import us.coletnelson.colang.builders.ColangBuilder;
import us.coletnelson.colang.generated.ColangBaseListener;
import us.coletnelson.colang.generated.ColangParser;

public class ColangJavaListener extends ColangBaseListener {
	
	private static final int DEFAULT_MAX_ITR_TIMEOUT = -1;
	
	private ColangBuilder builder;
	private int maxItrTimeout;
	private boolean hasErrors;
	
	public ColangJavaListener() {
		this(DEFAULT_MAX_ITR_TIMEOUT);
	}
	
	public ColangJavaListener(int maxIterTimeout) {
		this.builder = new ColangBuilder();
		this.maxItrTimeout = maxIterTimeout;
		this.hasErrors = false;
	}
	
	public boolean hasErrors() {
		return this.hasErrors;
	}
	
	public ColangBuilder getBuilder() {
		return builder;
	}
	
	public boolean hasMaxItrTimeout() {
		return (this.maxItrTimeout != DEFAULT_MAX_ITR_TIMEOUT);
	}
	
	@Override public void enterProg(ColangParser.ProgContext ctx) {
		builder.appendln(String.format("/** Transpiled Colang Program %s **/", new Date().toString()));
		builder.appendln("import java.util.Scanner;");
		builder.appendln("public class ColangProgram {");
		builder.appendln("public static void main(String[] args) {");
		builder.appendln("Scanner sysin = new Scanner(System.in);");
		if(this.hasMaxItrTimeout()) {
			builder.appendln("int infLoopStopper = 0;");
		}
	}

	@Override public void exitProg(ColangParser.ProgContext ctx) {
		builder.appendln("sysin.close();");
		builder.appendln("}");
		builder.appendln("}");
	}

	@Override public void enterStmt(ColangParser.StmtContext ctx) { }

	@Override public void exitStmt(ColangParser.StmtContext ctx) { }

	@Override public void enterCond(ColangParser.CondContext ctx) {	}

	@Override public void exitCond(ColangParser.CondContext ctx) { }

	@Override public void enterBooleanExpr(ColangParser.BooleanExprContext ctx) {
		if(ctx.TRUE() != null) {
			builder.append("true");
		} else if(ctx.FALSE() != null) {
			builder.append("false");
		}
	}

	@Override public void exitBooleanExpr(ColangParser.BooleanExprContext ctx) { }

	@Override public void enterExpr(ColangParser.ExprContext ctx) {
		if(ctx.ID() != null) {
			builder.append(ctx.ID().toString());
		} else if(ctx.NUMBER() != null) {
			builder.append(ctx.NUMBER().toString());
		}
	}

	@Override public void exitExpr(ColangParser.ExprContext ctx) { }

	@Override public void enterInitStmt(ColangParser.InitStmtContext ctx) {
		builder.append("long " + ctx.ID() + " = ");
	}

	@Override public void exitInitStmt(ColangParser.InitStmtContext ctx) {
		builder.appendln(";");
	}

	@Override public void enterAssignStmt(ColangParser.AssignStmtContext ctx) {
		builder.append(ctx.ID() + " = ");
	}

	@Override public void exitAssignStmt(ColangParser.AssignStmtContext ctx) {
		builder.appendln(";");
	}

	@Override public void enterInputStmt(ColangParser.InputStmtContext ctx) {
		builder.appendln(ctx.ID() + " = sysin.nextInt();");
	}

	@Override public void exitInputStmt(ColangParser.InputStmtContext ctx) { }

	@Override public void enterOutputStmt(ColangParser.OutputStmtContext ctx) {
		if(ctx.OUTPUTLN() != null) {
			if(ctx.ID() != null) {
				builder.appendln("System.out.println(" + ctx.ID() + ");" );
			} else if(ctx.NUMBER() != null) {
				builder.appendln("System.out.println(" + ctx.NUMBER() + ");" );
			} else if(ctx.STRING() != null) {
				builder.appendln("System.out.println(" + ctx.STRING().toString().replaceAll("'", "\"") + ");" );
			} else {
				builder.appendln("System.out.println();");
			}
		} else {
			if(ctx.ID() != null) {
				builder.appendln("System.out.print(" + ctx.ID() + ");" );
			} else if(ctx.NUMBER() != null) {
				builder.appendln("System.out.print(" + ctx.NUMBER() + ");" );
			} else if(ctx.STRING() != null) {
				builder.appendln("System.out.print(" + ctx.STRING().toString().replaceAll("'", "\"") + ");" );
			}
		}
		
	}

	@Override public void exitOutputStmt(ColangParser.OutputStmtContext ctx) { }

	@Override public void enterWhileBlock(ColangParser.WhileBlockContext ctx) { 
		if(this.hasMaxItrTimeout()) {
			builder.append("infLoopStopper = 0;");
		}
		builder.append("while (");
	}

	@Override public void exitWhileBlock(ColangParser.WhileBlockContext ctx) {
		builder.appendln("}");
	}

	@Override public void enterIfBlock(ColangParser.IfBlockContext ctx) {
		builder.append("if(");
	}

	@Override public void exitIfBlock(ColangParser.IfBlockContext ctx) {
		builder.appendln("}");
	}

	@Override public void enterElseBlock(ColangParser.ElseBlockContext ctx) { }

	@Override public void exitElseBlock(ColangParser.ElseBlockContext ctx) { }

	@Override public void enterEveryRule(ParserRuleContext ctx) { }

	@Override public void exitEveryRule(ParserRuleContext ctx) { }

	@Override public void visitTerminal(TerminalNode node) {
		if(node.getText().equals("then")) {
			if(this.hasMaxItrTimeout()) {
				builder.appendln(") { if (infLoopStopper++ > " + this.maxItrTimeout + ") { return; }");
			} else {
				builder.appendln(") {");
			}
		} else if(node.getText().equals("else")) {
			builder.appendln("} else {");
		} else if(node.getText().equals("<")) {
			builder.append("<");
		} else if(node.getText().equals("<=")) {
			builder.append("<=");
		} else if(node.getText().equals(">")) {
			builder.append(">");
		} else if(node.getText().equals(">=")) {
			builder.append(">=");
		} else if(node.getText().equals("=")) {
			builder.append("==");
		} else if(node.getText().equals("+")) {
			builder.append("+");
		} else if(node.getText().equals("-")) {
			builder.append("-");
		} else if(node.getText().equals("*")) {
			builder.append("*");
		} else if(node.getText().equals("/")) {
			builder.append("/");
		} else if(node.getText().equals("(")) {
			builder.append("(");
		} else if(node.getText().equals(")")) {
			builder.append(")");
		}
	}

	@Override public void visitErrorNode(ErrorNode node) {
		this.hasErrors = true;
	}
}
