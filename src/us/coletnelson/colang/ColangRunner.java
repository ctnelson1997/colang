package us.coletnelson.colang;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import us.coletnelson.colang.builders.ColangSynBuilder;
import us.coletnelson.colang.builders.syn.dom.ColangSynModel;
import us.coletnelson.colang.builders.syn.dom.ColangSynReplacement;
import us.coletnelson.colang.generated.ColangLexer;
import us.coletnelson.colang.generated.ColangParser;
import us.coletnelson.colang.listeners.ColangJavaListener;
import us.coletnelson.colang.listeners.ColangJavaSynListener;

public class ColangRunner {

	private static final String DEFAULT_INFILENAME = "/colang/src/colang/input/ColangProgram.colang";
	private static final String DEFAULT_OUTFILEDIR = "/colang/src/colang/output/";

	private static final String DEFAULT_TESTDIR = "/colang/src/colang/tests/";
	private static final String DEFAULT_WORKDIR = "/colang/src/colang/work/";
	
	private static final int DEFAULT_PROJECT_TIMEOUT = 60;
	private static final int DEFAULT_FILE_ITR_TIMEOUT = 9999;
	private static final int DEFAULT_NUM_TESTS = 0;
	
	private static final boolean DEFAULT_DOSYNTHESIS = false;
	private static final boolean DEFAULT_DOEVALUATION = false;

	private static String inFileName = DEFAULT_INFILENAME;
	private static String outFileDir = DEFAULT_OUTFILEDIR;

	private static String testDir = DEFAULT_TESTDIR;
	private static String workDir = DEFAULT_WORKDIR;
	private static int projectTimeout = DEFAULT_PROJECT_TIMEOUT;
	private static int itrTimeout = DEFAULT_FILE_ITR_TIMEOUT;
	private static int numTests = DEFAULT_NUM_TESTS;
	
	private static boolean doSynthesis = DEFAULT_DOSYNTHESIS;
	private static boolean doEvaluation = DEFAULT_DOEVALUATION;

	public static void main(String[] args) throws Exception {
		parseArgs(args);
		String data = ColangUtil.readFile(inFileName);
		run(data);
	}

	private static void run(String data) throws Exception {
		ColangLexer lexer = new ColangLexer(new ANTLRInputStream(data));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		ColangParser parser = new ColangParser(tokens);
		ColangParser.ProgContext context = parser.prog();

		if (doSynthesis) {
			synthesize(context);
		} else if (doEvaluation) {
			evaluate(context);
		} else {
			run(context);
		}
	}

	private static void parseArgs(String[] args) {
		for (String arg : args) {
			if ("synthesize".equals(arg)) {
				doSynthesis = true;
			} else if ("evaluate".equals(arg)) {
				doEvaluation = true;
			} else if (arg != null) {
				String[] argPair = arg.split("=");
				if (argPair.length == 2) {
					if ("in".equals(argPair[0])) {
						inFileName = argPair[1];
					} else if ("out".equals(argPair[0])) {
						outFileDir = argPair[1];
					} else if ("tests".equals(argPair[0])) {
						testDir = argPair[1];
					} else if ("work".equals(argPair[0])) {
						workDir = argPair[1];
					} else if ("maxTime".equals(argPair[0])) {
						projectTimeout = Integer.parseInt(argPair[1]);
					} else if ("maxItr".equals(argPair[0])) {
						itrTimeout = Integer.parseInt(argPair[1]);
					} else if ("numTests".equals(argPair[0])) {
						numTests = Integer.parseInt(argPair[1]);
					}
				}
			}
		}
	}

	public static void run(ColangParser.ProgContext context) throws IOException {
		ParseTreeWalker walker = new ParseTreeWalker();

		ColangJavaListener listener = new ColangJavaListener();
		walker.walk(listener, context);

		if(listener.hasErrors()) {
			System.out.println("Encountered incorrect syntax, check your code again!");
		} else {
			ColangUtil.writeFile(outFileDir + ColangUtil.getDefaultNameWithJavaExt(), listener.getBuilder().getPretty());
			ColangUtil.executeJavaProgram(outFileDir, ColangUtil.getDefaultName());
		}
	}
	
	public static void evaluate(ColangParser.ProgContext context) throws Exception {
		ParseTreeWalker walker = new ParseTreeWalker();

		ColangJavaListener listener = new ColangJavaListener(itrTimeout);
		walker.walk(listener, context);
		
		if(listener.hasErrors()) {
			System.out.println(false);
		} else {
			ColangUtil.writeFile(outFileDir + ColangUtil.getDefaultNameWithJavaExt(), listener.getBuilder().getPretty());
			ColangUtil.writeFile(workDir + ColangUtil.getDefaultNameWithJavaExt(), listener.getBuilder().getPretty());

			boolean result = ColangSynthesizer.evaluate(workDir, testDir, numTests, ColangUtil.getDefaultName());
			
			System.out.println(result);
		}
	}

	public static void synthesize(ColangParser.ProgContext context) throws Exception {
		ParseTreeWalker walker = new ParseTreeWalker();

		ColangJavaSynListener listener = new ColangJavaSynListener(itrTimeout);
		walker.walk(listener, context);
		// Only proceed with the synthesis if there are invocations that can be
		// replaced.
		if(listener.hasErrors()) {
			System.out.println("Encountered incorrect syntax, check your code again!");
			finalizeNoSolutionFound();
		} else if (listener.getBuilder().hasReplaceables()) {
			// Construct both the program and string models
			List<ColangSynModel> programModels = listener.getBuilder().getReplacementsWithModels();
			List<String> strProgramModels = new ArrayList<>();
			programModels.forEach(programModel -> {
				strProgramModels.add(programModel.getData());
			});
			for (int i = 1; i <= strProgramModels.size(); i++) {
				ColangUtil.writeFile(workDir + ColangUtil.getNameWithExt(i),
						strProgramModels.get(i - 1).replace(ColangUtil.getDefaultName(), ColangUtil.getName(i)));
			}

			// Attempt to find a solution in the search space with the given time constraint
			Integer solutionNum = ColangSynthesizer.synthesize(workDir, testDir, projectTimeout, programModels.size(), numTests);
			if (solutionNum == null) {
				System.out.println("No solution found!");
				finalizeNoSolutionFound();
			} else {
				finalizeSynthesizedSolution(solutionNum, programModels);
			}
		} else {
			System.out.println("No replaceables found -- cannot synthesize without invocations!");
			finalizeNoSolutionFound();
		}
	}

	private static void finalizeSynthesizedSolution(int solutionNum, List<ColangSynModel> programModels)
			throws Exception {
		
		System.out.println();
		System.out.println("Solution " + ColangUtil.getNameWithExt(solutionNum));
		System.out.println();
		
		ColangSynModel solutionModel = programModels.get(solutionNum - 1);
		
		System.out.println("FIXES");
		printReplacementResults(solutionModel.getPair().getReplacement1());
		if(!solutionModel.isSingle()) {
			printReplacementResults(solutionModel.getPair().getReplacement2());
		}
		
		System.out.println("SOLUTION FOUND");
		String colangProgram = ColangUtil.readFile(inFileName);

		// Insert the repairs at their original points in the buggy .colang program
		ColangSynReplacement replacement1 = solutionModel.getPair().getReplacement1();
		String[] colangProgramLines = colangProgram.split("\r?\n");
		int lineNum = replacement1.getSynPosition().getLineNumber() - 1;
		colangProgramLines[lineNum] = repair(colangProgramLines[lineNum],
				replacement1.getSynPosition().getColumnNumber(), replacement1.getOriginalData(),
				replacement1.getNewData());
		
		// Note, we have to do some additional calculations if the second repairs are located
		// on the same line number (adjust the column number)
		if(!solutionModel.isSingle()) {
			ColangSynReplacement replacement2 = solutionModel.getPair().getReplacement2();
			lineNum = replacement2.getSynPosition().getLineNumber() - 1;
			if (replacement1.getSynPosition().getLineNumber() == replacement2.getSynPosition().getLineNumber()
					&& replacement1.getSynPosition().getColumnNumber() < replacement2.getSynPosition().getColumnNumber()) {
				colangProgramLines[lineNum] = repair(colangProgramLines[lineNum],
						(replacement1.getNewData().length() - replacement1.getOriginalData().length())
								+ replacement2.getSynPosition().getColumnNumber(),
						replacement2.getOriginalData(), replacement2.getNewData());
			} else {
				colangProgramLines[lineNum] = repair(colangProgramLines[lineNum],
						replacement2.getSynPosition().getColumnNumber(), replacement2.getOriginalData(),
						replacement2.getNewData());
			}
		}
		
		// Tada
		colangProgram = "";
		for (String line : colangProgramLines) {
			colangProgram += line + "\n";
		}
		System.out.println(colangProgram);

		printRepairs(solutionModel);
		
		ColangUtil.writeFile(outFileDir + ColangUtil.getDefaultNameWithColangExt(), colangProgram);
	}
	
	private static void finalizeNoSolutionFound() throws IOException {
		ColangUtil.writeFile(outFileDir + ColangUtil.getDefaultNameWithColangExt(), "outputln 'Hello world!'");
		ColangUtil.writeFile(outFileDir + "Repairs.json", "{}");
	}
	
	// Could/Should generalize to n replacements
	private static void printRepairs(ColangSynModel solutionModel) throws IOException {
		List<String> allRepairs = new ArrayList<>();
		String repairs1 = extractRepairs(solutionModel.getPair().getReplacement1());
		
		if(!repairs1.isEmpty()) {
			allRepairs.add(repairs1);
		}
		
		if(!solutionModel.isSingle()) {
			String repairs2 = extractRepairs(solutionModel.getPair().getReplacement2());
			if(!repairs2.isEmpty()) {
				allRepairs.add(repairs2);
			}
		}
		
		String repairs = "{";
		if(allRepairs.size() > 0) {
			repairs += "\n\t\"repair1\": " + allRepairs.get(0);
		}
		if(allRepairs.size() > 1) {
			repairs += ",\n\t\"repair2\": " + allRepairs.get(1) + "\n";
		}
		repairs += "\n}";
		
		ColangUtil.writeFile(outFileDir + "Repairs.json", repairs);
	}
	
	private static String extractRepairs(ColangSynReplacement replacement) {
		if(!replacement.getOriginalData().equals(replacement.getNewData())) {
			return String.valueOf(replacement.getSynPosition().getLineNumber());
		} else {
			return "";
		}
	}
	
	private static void printReplacementResults(ColangSynReplacement replacement) {
		if(!replacement.getOriginalData().equals(replacement.getNewData())) {
			System.out.println(String.format("Replace the %d invocation of %s with %s",
					replacement.getPosition(),
					replacement.getOriginalData(),
					replacement.getNewData()));
			System.out.println("Found on line " + replacement.getSynPosition().getLineNumber());
			System.out.println();
		}
	}

	private static String repair(String line, int charPos, String originalWord, String newWord) {
		return line.substring(0, charPos) + newWord + line.substring(charPos + originalWord.length(), line.length());
	}
}