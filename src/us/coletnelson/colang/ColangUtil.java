package us.coletnelson.colang;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import java.util.Scanner;

public class ColangUtil {
	
	public static final String SOURCE_EXT = ".colang";
	public static final String TARGET_EXT = ".java";
	
	private static final String PREFIX = "ColangProgram";
	private static final String NUMZEROES = "0000000";
	
	public static String getDefaultName() {
		return PREFIX;
	}
	
	public static String getDefaultNameWithJavaExt() {
		return getDefaultName() + TARGET_EXT;
	}
	
	public static String getDefaultNameWithColangExt() {
		return getDefaultName() + SOURCE_EXT;
	}
	
	public static String getName(int i) {
		return (PREFIX + (NUMZEROES + i).substring(String.valueOf(i).length()));
	}
	
	public static String getNameWithExt(int i) {
		return getName(i) + TARGET_EXT;
	}
	
	public static String readFile(String name) throws FileNotFoundException {
		File file = new File(name);
		Scanner input = new Scanner(file);
		String data = "";
		while (input.hasNextLine()) {
			data += input.nextLine() + "\n";
		}
		input.close();
		return data;
	}
	
	public static void writeFile(String name, String data) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(name));
		writer.write(data);
		writer.close();
	}
	
	public static void executeJavaProgram(String dir, String name) {
		try {
			Process process = new ProcessBuilder("javac", dir + "ColangProgram.java")
					.redirectOutput(ProcessBuilder.Redirect.INHERIT)
					.redirectInput(ProcessBuilder.Redirect.INHERIT)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .start();
			process.waitFor();
			
			process = new ProcessBuilder("java", "-cp", dir, name)
					.redirectOutput(ProcessBuilder.Redirect.INHERIT)
					.redirectInput(ProcessBuilder.Redirect.INHERIT)
                    .redirectError(ProcessBuilder.Redirect.INHERIT)
                    .start();
            process.waitFor();
            
		} catch (IOException | InterruptedException e) {
			return;
		}
	}
}
