package us.coletnelson.colang;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ColangSynthesizer {

	private static List<String> inputs = new ArrayList<>();
	private static List<String> outputs = new ArrayList<>();

	public static boolean evaluate(String workDir, String testDir, int numTests, String programName) throws Exception {
		readTestData(testDir, numTests);
		
		if(!compile(workDir)) {
			return false;
		}

		if (new Solution(programName, workDir, numTests).passes()) {
			return true;
		} else { 
			return false;
		}
	}

	public static Integer synthesize(String workDir, String testDir, int timeout, int numPrograms, int numTests)
			throws Exception {

		System.out.println("Possible fixes: " + numPrograms);

		long startTime = System.currentTimeMillis();

		readTestData(testDir, numTests);

		if(!compile(workDir)) {
			return null;
		}

		for (int i = 1; i <= numPrograms; i++) {
			System.out.println("Attempting fix " + i);
			if (hasTimedOut(startTime, timeout)) {
				return null;
			} else if (new Solution(ColangUtil.getName(i), workDir, numTests).passes()) {
				return i;
			}
		}
		return null;
	}

	private static boolean hasTimedOut(long startTime, int timeout) {
		return ((System.currentTimeMillis() - startTime) / 1000 > timeout);
	}

	private static void readTestData(String testDir, int numTests) throws IOException {
		for (int i = 1; i <= numTests; i++) {
			inputs.add(ColangUtil.readFile(testDir + i + ".in").trim());
			outputs.add(ColangUtil.readFile(testDir + i + ".out").trim());
		}
	}

	private static boolean compile(String workDir) throws IOException, InterruptedException {
		// WINDOWS
		// Process process = new ProcessBuilder("javac", workDir + "*.java").start();

		// LINUX
		Process process = new ProcessBuilder("bash", "-c", "javac " + workDir + "*.java").start();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
		String errOutput = "";
		String currLine = null;
		while ((currLine = br.readLine()) != null) {
			errOutput += currLine + "\n";
		}
		process.waitFor();

		if(errOutput.trim().isEmpty()) {
			return true;
		} else {
			System.out.println("Compilation of fixes failed! There is likely a syntax error.");
			return false;
		}
		
	}

	private static class Solution {

		private String name;
		private String workDir;
		private int numTests;

		public Solution(String name, String workDir, int numTests) {
			this.name = name;
			this.workDir = workDir;
			this.numTests = numTests;
		}

		public boolean passes() {
			for (int i = 1; i <= numTests; i++) {
				if (!(new Test(name, i, workDir).passses())) {
					return false;
				}
			}
			return true;
		}

	}

	private static class Test {

		private String name;
		private int i;
		private String workDir;

		public Test(String name, int i, String workDir) {
			this.name = name;
			this.i = i;
			this.workDir = workDir;
		}

		public boolean passses() {
			try {
				Process process = new ProcessBuilder("java", "-cp", workDir, name).start();
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
				BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));

				String input = inputs.get(i - 1);
				String[] inputLines = input.split("\n");
				for (String inputLine : inputLines) {
					bw.write(inputLine);
					bw.newLine();
				}
				bw.newLine();
				bw.close();

				String currLine = null;
				String output = "";
				while ((currLine = br.readLine()) != null) {
					output += currLine + "\n";
				}
				output = output.trim();

				process.waitFor();

				if (output.equals(outputs.get(i - 1))) {
					return true;
				} else {
					return false;
				}
			} catch (IOException | InterruptedException e) {
				return false;
			}
		}

	}
}
