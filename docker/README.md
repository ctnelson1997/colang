To build, open a terminal in this directory and execute...
`docker build -f Dockerfile .. -t ctnelson1997/colang:latest`

To run, open a terminal in any directory and execute...
`docker run -it --name colang ctnelson1997/colang:latest /bin/sh`