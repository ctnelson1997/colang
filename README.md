# Colang

Colang is a simple programming language with built-in mechanisms for synthesizing program repairs on pairwise boundary cases. It is to be used in correlation with Snoot, a frontend for "sniffing out" program repairs.

This project was completed as a part of an independent project for CS703: Program Verification and Synthesis at UW-Madison.

Video Demonstration: https://www.youtube.com/watch?v=OpGQ9UhF-BE

## Programming Language

Colang is a simple, procedural programming language with a handful of constructs. These are listed below, and examples are appended to the bottom of this README file.

| Construct                                                  | Description                                                  |
| ---------------------------------------------------------- | ------------------------------------------------------------ |
| `initialize {variable} as {expression}`                    | Initializes `variable` with value evaluated from `expression`. Note that variables can *only* hold numeric values (integers and floating point numbers) in Colang. |
| `assign {expression} to {variable}`                        | Assigns a value evaluated from `expression` to `variable`. Note that variables can *only* hold numeric values (integers and floating point numbers) in Colang. |
| `input {variable}`                                         | Takes user input and assigns it to `variable`. Note that variables can *only* hold numeric values (integers and floating point numbers) in Colang. |
| `output {value}`                                           | Outputs `value`, which can take the form of a `variable`, `number`, or `string_literal` to the standard output. |
| `outputln {value}`                                         | Same as `output`, but appends a newline to the end of the output. |
| `while {condition} then {statements} end`                  | Executes a list of statements `statements` until `condition` evaluates to false. |
| `if {condition} then {statements} end`                     | Conditionally executes a list of statements `statements` if `condition` evaluates to true. |
| `if {condition} then {statements1} else {statements2} end` | Conditionally executes a list of statements `statements1` if `condition` evaluates to true, or executes a list of statements `statements2` if `condition` evaluates to false. |
| `{condition}`                                              | A boolean condition that accepts `true`, `false`, or boolean expressions in the form of `{expr1} {boolop} {expr2}` where `expr1` and `expr2` are expressions and `boolop` is a boolean operation `=`, `<`,`>`, `<=`, `>=`. Multiple `condition` can be chained together using `and` or `or`. |
| `{expression}`                                             | An expression is considered the addition `+`, subtraction `-`, multiplication `*`, or division `/` of any two numbers, expressions, or variables. |
| `{statement}`                                              | A statement is defined as either an `expression`, `input`, `output`, `assign`,`initialize`, `while`, or `if/else` statement. |



## Program Arguments

Below is the table of modes and properties that Colang can run on. Note that at most one mode can be specified.

| Type     | Argument              | Description                                                  |
| -------- | --------------------- | ------------------------------------------------------------ |
| mode     | `synthesize`          | Runs the Colang program in synthesis mode. Based on the input program and set of test cases, it will attempt all pairs of boundary values that could result in a working program. Upon a successful find, it will provide a list of fixes as well as the repaired program, with more details in the output folder. Requires `in`, `out`, `work`, `tests`, and `numTests` to be specified. `maxTime` and `maxItr` are optional and default at `60` seconds or `9999` iterations by default. |
| mode     | `evaluate`            | Runs the Colang program in evaluation mode. Based on the input program and set of test cases, it will evaluate to `true` or `false` on whether or not the program passes the test cases. Requires `in`, `out`, `work`, `tests`, and `numTests` to be specified. `maxTime` and `maxItr` are optional and default at `60` seconds or `9999` iterations by default. |
| mode     | `{none_specified}`    | If neither `synthesize` or `evaluate` are specified, Colang will run in it's default execution mode. That is, the Colang program will be transpiled to Java and ran. Requires `in`, `out`, and `work`. `maxTime` and `maxItr` are optional and default at `60` seconds or `9999` iterations by default. |
| property | `in={FILE.colang}`    | Specifies the file and path for the input Colang program.    |
| property | `out={out_dir}`       | Specifies the output directory for any transpiled, synthesized, or evaluated Colang program. |
| property | `work={work_dir}`     | Specifies the directory in which Colang is allowed to transpile `.colang` files into `.java` files, compile `.java` files into `.class` files, and run the transpiled code. This is not a directory for deliverables; simply somewhere for Colang to work. |
| property | `tests={tests_dir}`   | Specifies the directory of tests to evaluate on for performing `synthesize` or `evaluate` modes. Note this directory must have pairs of `.in` and `.out` files beginning from `0`, e.g. `0.in`, `0.out`, `1.in`, `1.out`, etc. |
| property | `numTests={numTests}` | Specifies the number of tests to run from the `tests` property. Test cases `[1,numTests]` will be executed as a part of `synthesize` or `evaluate`. |
| property | `maxTime={maxTime}`   | Specifies the maximum amount of time Colang is allowed to spend on a set of programs within `synthesize` or `evaluate` before giving up. By default, this is set to `60` seconds. |
| property | `maxItr={maxItr}`     | Specifies the maximum number of iterations Colang is allowed to spend on a single program within`synthesize` or `evaluate` before giving up. By default, this is set to `9999` iterations. |



## Docker

For your convenience, Colang is available as a Docker image from `ctnelson1997/colang:latest`. You can execute the following commands to start it on your own machine.

`docker pull ctnelson1997/colang:latest`

`docker run -it --name colang ctnelson1997/colang:latest /bin/sh`

**Note**: If the container is running or has been ran before you may need to execute `docker stop colang && docker rm colang`.

### Commands

You can run Colang programs and specify program arguments using `colangopt {your_args}`, replacing `{your_args}` with the appropriate arguments from above. Alternatively, you can run `colang {yourfile.colang}` to run a Colang program directly, e.g. `colang HelloWorld.colang`.

### Examples

Some examples have been provided in the Docker image underneath `examples`. You can navigate to this directory by doing `cd examples` and executing a program such as `colang PerformFactorial.colang`. Alternatively, you can navigate to `synthesis_example` which can be evaluated and synthesized with...

`colangopt evaluate in="BrokenPerformFactorial.colang" out="/colang/src/colang/output/" work="/colang/src/colang/work/" tests="./tests/" numTests=4`

`colangopt synthesize in="BrokenPerformFactorial.colang" out="/colang/src/colang/output/" work="/colang/src/colang/work/" tests="./tests/" numTests=4`



## Programming Examples

### Hello World

Prints "Hello World!" to the screen.

```
outputln 'Hello World!'
```



### Factorial

Performs and prints the factorial of an inputted number.

```
initialize number as 0

output 'Enter a number to take the factorial of '
input number

if number <= 1 then
	output '1'
else
	initialize i as 1
	initialize result as 1

	while i <= number then
		assign i * result to result
		assign i + 1 to i
	end

	outputln result
end
```



### Starcase

Performs and prints the starcase of an inputted number.

```
initialize numStars as 0

output 'Enter the number of steps to print '
input numStars
outputln

initialize i as 0
initialize j as 0

while i <= numStars then
	assign 0 to j
	while j < i then
		output '*'
		assign j + 1 to j
	end
	outputln
	assign i + 1 to i
end
```



### Year

Given a year, prints what the next year will be.

```
outputln 'What year is it?'

initialize year as 0
initialize nextYear as 0

input year

if year = 2020 then
	assign 2020 to nextYear
else
	assign year + 1 to nextYear
end

output 'Nice, next year it will be '
output nextYear
outputln '.'
```

