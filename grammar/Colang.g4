grammar Colang;		
prog:	NEWLINE* (stmt)* NEWLINE* EOF;
stmt:	(expr | initStmt | assignStmt | inputStmt | outputStmt | whileBlock | ifBlock) NEWLINE+;
cond:	booleanExpr ((AND|OR) booleanExpr)*;
booleanExpr: expr (EQ|LT|GT|LTE|GTE) expr | TRUE | FALSE;
expr:	expr ('*'|'/') expr
    |	expr ('+'|'-') expr
    |	NUMBER
	|	ID
    |	'(' expr ')'
    ;
	
initStmt: INITIALIZE ID AS expr;
assignStmt: ASSIGN expr TO ID;
inputStmt: INPUT ID;
outputStmt: OUTPUT (ID|NUMBER|STRING) | OUTPUTLN (ID|NUMBER|STRING)?;

whileBlock: WHILE cond THEN NEWLINE (stmt)* END;
ifBlock: IF cond THEN NEWLINE (stmt)* (END|elseBlock);
elseBlock: ELSE NEWLINE (stmt)* END;

ASSIGN: 'assign';
TO: 'to';
INITIALIZE: 'initialize';
AS: 'as';

INPUT: 'input';
OUTPUT: 'output';
OUTPUTLN: 'outputln';

WHILE: 'while';
IF: 'if';
ELSE: 'else';
THEN: 'then';
END: 'end';

TRUE: 'true';
FALSE: 'false';
AND: 'and';
OR: 'or';

EQ: '=';
LT: '<';
GT: '>';
LTE: '<=';
GTE: '>=';

NEWLINE : [\r\n]+ ;
NUMBER  : [0-9]+(.[0-9]+)?;
STRING  :   '\'' ( Escape | ~('\'' | '\\' | '\n' | '\r') ) + '\'';
fragment Escape : '\\' ( '\'' | '\\' );
ID: [a-zA-Z0-9_]+;

WS: [ \t]+ -> skip;